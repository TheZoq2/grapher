red = '#FF0000'; green = '#00FF00'; blue = '#0000FF'; skyblue = '#87CEEB';
set style data histogram
set style histogram cluster gap 1
set style fill solid
set boxwidth 0.9
set xtics format ''
set grid ytics

$data << EOD
7 10565 8225 9562
5 4828 4828 4959
6 7475 5962 4997
4 4551 4551 4699
3 1444 1444 1444
2 645 645 645
1 651 651 651
EOD


set title 'A Sample Bar Chart'
plot 'demo_data.dat' using 2 title 'Dan' linecolor rgb red,   \
     'demo_data.dat' using 3 title 'Sophia' linecolor rgb blue,   \
     'demo_data.dat' using 4 title 'Jody' linecolor rgb green,    \
