use crate::traits::Chart;

const TEMPLATE: &'static str = r#"
set style data histogram
set style histogram cluster gap 1
set style fill solid
set boxwidth 0.9
set xtics format ''
set title "${TITLE}"
set grid ytics
$data << EOD
${DATA}
EOD

${PLOT_CMD}
pause mouse close
"#;

pub fn generate_gnuplot<T: Chart + Sized>(
    title: &str,
    data: &[T],
    value_fn: impl Fn(&T) -> f64,
    relative_values: bool,
) -> String {
    let gnuplot_data = Chart::gnuplot_data(&data, value_fn, relative_values);

    let colors = [
        "'#66c2a4'",
        "'#2ca25f'",
        "'#b2e2e2'",
        "'#006d2c'",
        "'#fc8d59'",
        "'#e34a33'",
        "'#fdcc8a'",
        "'#b30000'",
        "'#969696'",
        "'#636363'",
        "'#cccccc'",
        "'#252525'",
        "'#df65b0'",
        "'#dd1c77'",
        "'#d7b5d8'",
        "'#980043'",
        "'#74a9cf'",
        "'#2b8cbe'",
        "'#bdc9e1'",
        "'#045a8d'",
        "'#fe9929'",
        "'#d95f0e'",
        "'#fed98e'",
        "'#993404'",
    ];

    let plot_cmds = Chart::categories(&data)
        .iter()
        .map(T::category_label)
        .enumerate()
        .map(|(i, s)| {
            format!(
                "'$data' using {}:xtic(1) title '{}' linecolor rgb {}",
                // +2 because arrays are 1 indexed and the first index is the cluster title
                i + 2,
                s,
                colors[i % colors.len()]
            )
        })
        .collect::<Vec<_>>();

    let plot_cmd = format!("plot {}", plot_cmds.join(", "));

    TEMPLATE
        .replace("${DATA}", &gnuplot_data)
        .replace("${PLOT_CMD}", &plot_cmd)
        .replace("${TITLE}", title)
}
