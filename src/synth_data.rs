use std::{collections::HashMap, fmt};

use lazy_static::lazy_static;
use regex::Regex;
use serde::{Deserialize, Deserializer};

use crate::traits::Chart;

const IGNORED_FLAGS: &'static [&'static str] = &["--compress-inputs", "-C-DSEQUENTIAL_PARTS"];

fn deserialize_flags<'de, D>(de: D) -> Result<Vec<String>, D::Error>
where
    D: Deserializer<'de>,
{
    struct FlagVisitor();

    impl<'de> serde::de::Visitor<'de> for FlagVisitor {
        /// Return type of this visitor. This visitor computes the max of a
        /// sequence of values of type T, so the type of the maximum is T.
        type Value = Vec<String>;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("a string of space separated flags")
        }

        fn visit_str<E>(self, v: &str) -> Result<Self::Value, E> {
            Ok(v.split(" ").into_iter().map(String::from).collect())
        }
    }

    // Create the visitor and ask the deserializer to drive it. The
    // deserializer will call visitor.visit_seq() if a seq is present in
    // the input data.
    let visitor = FlagVisitor();
    de.deserialize_str(visitor)
}

#[derive(Debug, Deserialize)]
pub struct QuartusSynthData {
    pub model_name: String,
    #[serde(deserialize_with = "deserialize_flags")]
    pub opt_flags: Vec<String>,
    pub opt_level: String,
    pub wl: i32,
    pub alms: i32,
    pub luts: i32,
    pub regs: i32,
    pub luts_and_regs: i32,
    // pub brams: i32,
    // pub dsps: i32,
    // pub freq: f32,
    pub seed: Option<i32>,
}

pub struct PartSynth {
    pub data: QuartusSynthData,
}

impl PartSynth {
    pub fn new(data: QuartusSynthData) -> Self {
        Self { data }
    }
}

fn category_from_flags(flags: &[String]) -> String {
    let cleaned_flags = flags
        .iter()
        .filter(|flag| !IGNORED_FLAGS.contains(&flag.as_str()))
        .collect::<Vec<_>>();

    match cleaned_flags
        .into_iter()
        .map(String::as_str)
        .collect::<Vec<_>>()
        .as_slice()
    {
        [] => "cache".to_string(),
        ["--no-sig-path-cache"] => "no cache".to_string(),
        ["--phi-masks"] => "masks".to_string(),
        other => panic!("Unrecognised flag combination: {:?}", other),
    }
}

lazy_static! {
    static ref PART_RE: Regex = Regex::new(r"-C=?-DPART(\d+)").unwrap();
}

impl Chart for PartSynth {
    type Cluster = String;
    type Category = Vec<String>;

    fn cluster(&self) -> Self::Cluster {
        self.data
            .opt_flags
            .iter()
            .cloned()
            .filter(|f| PART_RE.is_match(f))
            .next()
            .expect("Expected at least one part flag")
    }
    fn category(&self) -> Self::Category {
        self.data
            .opt_flags
            .iter()
            .cloned()
            .filter(|f| !PART_RE.is_match(f))
            .collect()
    }

    fn cluster_label(raw_cluster: &String) -> String {
        PART_RE.replace(&raw_cluster, "$1").to_string()
    }

    fn category_label(raw_category: &Self::Category) -> String {
        category_from_flags(raw_category)
    }

    fn sort_clusters(mut clusters: Vec<Self::Cluster>) -> Vec<Self::Cluster> {
        clusters.sort();
        clusters
    }
}

pub struct SeedSynth {
    pub data: QuartusSynthData,
}

impl SeedSynth {
    pub fn new(data: QuartusSynthData) -> Self {
        Self { data }
    }
}

impl Chart for SeedSynth {
    type Cluster = i32;
    type Category = Vec<String>;

    fn cluster(&self) -> Self::Cluster {
        self.data
            .seed
            .expect("attempting to get seed from record with no seed specified")
    }
    fn category(&self) -> Self::Category {
        self.data
            .opt_flags
            .iter()
            .cloned()
            .filter(|f| !PART_RE.is_match(f))
            .collect()
    }

    fn cluster_label(raw_cluster: &i32) -> String {
        format!("{}", raw_cluster)
    }

    fn category_label(raw_category: &Self::Category) -> String {
        category_from_flags(raw_category)
    }

    fn sort_clusters(mut clusters: Vec<Self::Cluster>) -> Vec<Self::Cluster> {
        clusters.sort();
        clusters
    }
}

pub struct WordLengthSynth {
    pub data: QuartusSynthData,
}

impl WordLengthSynth {
    pub fn new(data: QuartusSynthData) -> Self {
        Self { data }
    }
}

impl Chart for WordLengthSynth {
    type Cluster = i32;
    type Category = Vec<String>;

    fn cluster(&self) -> Self::Cluster {
        self.data.wl
    }
    fn category(&self) -> Self::Category {
        self.data
            .opt_flags
            .iter()
            .cloned()
            .filter(|f| !PART_RE.is_match(f))
            .collect()
    }

    fn cluster_label(raw_cluster: &i32) -> String {
        format!("{}", raw_cluster)
    }

    fn category_label(raw_category: &Self::Category) -> String {
        category_from_flags(raw_category)
    }

    fn sort_clusters(mut clusters: Vec<Self::Cluster>) -> Vec<Self::Cluster> {
        clusters.sort();
        clusters
    }
}

fn if_kind_synth_category_label(flags: &[String]) -> String {
    let phi_method = if flags.contains(&"--no-sig-path-cache".to_string()) {
        "no cache"
    } else if flags.contains(&"--phi-masks".to_string()) {
        "masks"
    } else {
        "cache"
    };

    let local_ignored_flags = [
        "--no-sig-path-cache",
        "--phi-masks",
        "-C-DPART5",
        "-pRemoveSingleSelect",
    ];

    let cleaned_flags = flags
        .into_iter()
        .filter(|flag| !IGNORED_FLAGS.contains(&flag.as_str()))
        .filter(|flag| !local_ignored_flags.contains(&flag.as_str()))
        .collect::<Vec<_>>();

    let flag_map = [
        ("--filter-undef", "filter"),
        ("-pSelectElseLast", "last"),
        ("-pSelectElseX", "X"),
        ("--no-retiming", "nore"),
        ("--recompute-not", "recomp. not"),
        ("--naive-paths", "naive"),
    ]
    .into_iter()
    .map(|(l, r)| (l.to_string(), r.to_string()))
    .collect::<HashMap<_, _>>();

    let other_options = cleaned_flags
        .into_iter()
        .map(|flag| {
            if let Some(descr) = flag_map.get(flag) {
                descr.clone()
            } else {
                panic!("Unrecognised flag: {}", flag)
            }
        })
        .collect::<Vec<_>>()
        .join("; ");

    format!("{}; {}", phi_method, other_options)
}

pub struct IfKindSynth {
    pub data: QuartusSynthData,
}

impl IfKindSynth {
    pub fn new(data: QuartusSynthData) -> Self {
        Self { data }
    }
}

impl Chart for IfKindSynth {
    type Cluster = String;
    type Category = Vec<String>;

    fn cluster(&self) -> Self::Cluster {
        self.data.model_name.clone()
    }

    fn category(&self) -> Self::Category {
        self.data.opt_flags.iter().cloned().collect()
    }

    fn cluster_label(raw_cluster: &String) -> String {
        format!("{}", raw_cluster)
    }

    fn category_label(raw_category: &Self::Category) -> String {
        if_kind_synth_category_label(raw_category)
    }

    fn sort_clusters(mut clusters: Vec<Self::Cluster>) -> Vec<Self::Cluster> {
        clusters.sort();
        clusters
    }
}

#[derive(Debug, Deserialize)]
pub struct VivadoSynthData {
    pub model_name: String,
    #[serde(deserialize_with = "deserialize_flags")]
    pub opt_flags: Vec<String>,
    pub opt_level: String,
    // pub wl: i32,
    // pub slice_luts: i32,
    // pub slice_registers: i32,
    pub slices: i32,
    pub slice_luts: i32,
    pub slice_registers: i32,
    pub seed: Option<i32>,
}

pub struct VivadoIfKindSynth {
    pub data: VivadoSynthData,
}

impl VivadoIfKindSynth {
    pub fn new(data: VivadoSynthData) -> Self {
        Self { data }
    }
}

impl Chart for VivadoIfKindSynth {
    type Cluster = String;
    type Category = Vec<String>;

    fn cluster(&self) -> Self::Cluster {
        self.data.model_name.clone()
    }

    fn category(&self) -> Self::Category {
        self.data.opt_flags.iter().cloned().collect()
    }

    fn cluster_label(raw_cluster: &String) -> String {
        format!("{}", raw_cluster)
    }

    fn category_label(raw_category: &Self::Category) -> String {
        if_kind_synth_category_label(raw_category)
    }

    fn sort_clusters(mut clusters: Vec<Self::Cluster>) -> Vec<Self::Cluster> {
        clusters.sort();
        clusters
    }
}



#[derive(Debug, Deserialize)]
pub struct NextpnrSynthData {
    pub model_name: String,
    #[serde(deserialize_with = "deserialize_flags")]
    pub opt_flags: Vec<String>,
    pub opt_level: String,

    // pub wl: i32,
    // pub slice_luts: i32,
    // pub slice_registers: i32,
    #[serde(rename(deserialize = "slices"))]
    pub slices: i32,
    #[serde(rename(deserialize = "lut4"))]
    pub lut4: i32,
    #[serde(rename(deserialize = "DFFs"))]
    pub dffs: i32,
    pub seed: Option<i32>,
}

pub struct NextpnrIfKindSynth {
    pub data: NextpnrSynthData
}

impl NextpnrIfKindSynth {
    pub fn new(data: NextpnrSynthData) -> Self {
        Self { data }
    }
}

impl Chart for NextpnrIfKindSynth {
    type Cluster = String;
    type Category = Vec<String>;

    fn cluster(&self) -> Self::Cluster {
        self.data.model_name.clone()
    }

    fn category(&self) -> Self::Category {
        self.data.opt_flags.iter().cloned().collect()
    }

    fn cluster_label(raw_cluster: &String) -> String {
        format!("{}", raw_cluster)
    }

    fn category_label(raw_category: &Self::Category) -> String {
        if_kind_synth_category_label(raw_category)
    }

    fn sort_clusters(mut clusters: Vec<Self::Cluster>) -> Vec<Self::Cluster> {
        clusters.sort();
        clusters
    }
}

// TODO: make a macro for the if kind synths
