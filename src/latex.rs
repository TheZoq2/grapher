use std::collections::HashMap;

use itertools::Itertools;
use serde::Deserialize;

use crate::traits::Chart;

#[derive(Deserialize)]
pub struct PlotProperties {
    pub caption: String,
    pub ylabel: String,
    pub label: String,
    pub bar_width: String,
}

#[derive(Deserialize)]
pub struct TableProperties {
    pub caption: String,
    pub label: String,
}

#[derive(Deserialize)]
pub struct SpecialProperties {
    pub plot: PlotProperties,
    pub table: TableProperties,
}

#[derive(Deserialize)]
pub struct CommonPlotProperties {
    /// Mapping from category label to legend
    pub legend_map: HashMap<String, String>,
    /// Mapping from cluster label to 'name'
    pub cluster_map: HashMap<String, String>,
    pub axis_args: String,
    pub relative_values: bool,
}

pub fn generate_tex_figure<T: Chart + Sized>(
    prop: &PlotProperties,
    cprop: &CommonPlotProperties,
    data: &[T],
    value_fn: &impl Fn(&T) -> f64,
) -> String {
    let categories = T::categories(data);
    let clusters = T::clusters(data);

    let cluster_label = |c| {
        let cleaned = T::cluster_label(c).replace("/", "").replace("_", "");
        cprop.cluster_map.get(&cleaned).cloned().unwrap_or(cleaned)
    };

    let x_coords = clusters
        .iter()
        .map(|c| (cluster_label)(c))
        .collect::<Vec<_>>()
        .join(",");

    let grid = T::build_grid(data, value_fn, cprop.relative_values);

    let addplot_cmds = categories
        .iter()
        .map(|category| {
            let coords = clusters
                .iter()
                .map(|cluster| {
                    let cluster_label = (cluster_label)(cluster).replace("_", "");
                    let value = grid
                        .get(&(cluster.clone(), category.clone()))
                        .cloned()
                        .unwrap_or_else(|| {
                            println!("Did not find a value for {cluster_label} {category:?}");
                            0.
                        });
                    format!("({cluster_label}, {value})")
                })
                .collect::<Vec<_>>()
                .join("");

            format!(r#"\addplot coordinates {{ {coords} }};"#)
        })
        .collect::<Vec<_>>()
        .join("\n");

    let legend = categories
        .iter()
        .map(|x| {
            let label = T::category_label(x);
            cprop.legend_map.get(&label).cloned().unwrap_or(label)
        })
        .collect::<Vec<_>>()
        .join(", ");

    format!(
        r#"
        \begin{{figure}}
        \begin{{tikzpicture}}
        \begin{{axis}}[
            ylabel={ylabel},
            symbolic x coords={{ {x_coords} }},
            {axis_args},
            bar width={bar_width}]
        {addplot_cmds}
        \legend{{{legend}}};
        \end{{axis}}
        \end{{tikzpicture}}
        \centering
        \caption{{{caption}}}
        \label{{{label}}}
        \end{{figure}}
    "#,
        ylabel = prop.ylabel,
        caption = prop.caption,
        axis_args = cprop.axis_args,
        label = prop.label,
        bar_width = prop.bar_width
    )
}

pub fn generate_tex_table<T: Chart + Sized>(
    prop: &TableProperties,
    cprop: &CommonPlotProperties,
    data: &[T],
    value_fn: &impl Fn(&T) -> f64,
) -> String {
    let categories = T::categories(data);
    let clusters = T::clusters(data);

    let table_spec = (0..=clusters.len()).map(|_| String::from("l")).join(" | ");

    let cluster_label = |c| {
        let cleaned = T::cluster_label(c).replace("/", "").replace("_", "");
        cprop.cluster_map.get(&cleaned).cloned().unwrap_or(cleaned)
    };
    let category_label = |x| {
        let label = T::category_label(x);
        cprop.legend_map.get(&label).cloned().unwrap_or(label)
    };

    let table_headers_inner = clusters
        .iter()
        .map(|cluster| {
            let header = (cluster_label)(cluster);
            format!("\\textbf{{{header}}}")
        })
        .join(" & ");

    let grid_relative = T::build_grid(data, value_fn, true);
    let grid_abs = T::build_grid(data, value_fn, false);

    let table_inner = categories
        .iter()
        .map(|category| {
            let inner = clusters
                .iter()
                .map(|cluster| {
                    let cluster_values = grid_abs
                        .iter()
                        .filter_map(|((clus, _), value)| if clus == cluster { Some(*value) } else { None })
                        .collect::<Vec<_>>();
                    let index = (cluster.clone(), category.clone());

                    let value_abs = grid_abs[&index];
                    let value_relative = grid_relative[&index] * 100.;

                    let is_best = value_abs == cluster_values
                            .iter()
                            .map(|x| *x)
                            .min_by(|x, y| x.partial_cmp(y).unwrap_or(std::cmp::Ordering::Equal))
                            .unwrap();

                    if is_best {
                        format!("\\textbf{{{value_abs}}} ({value_relative:.1} \\%)")
                    } else {
                        format!("{value_abs} ({value_relative:.1} \\%)")
                    }
                })
                .join("&");

            let label = (category_label)(category);
            format!("{label} & {inner} \\\\")
        })
        .join("\t\t\n");

    format!(
        r#"
        \begin{{table}}
            \caption{{{caption}}}\label{{{label}}}
            \begin{{tabular}}{{{table_spec}}}
                \textbf{{Method}} & {table_headers_inner} \\ \hline
                {table_inner}
            \end{{tabular}}
        \end{{table}}
    "#,
        caption = prop.caption,
        label = prop.label
    )
}

// Old table gen which generates one table per cluster with multiple values per
// table.
/*
/// Generates a tex table for each cluster.
pub fn generate_tex_tables<T: Chart + Sized>(
    prop: &TableProperties,
    cprop: &CommonPlotProperties,
    data: &[T],
    value_fns: Vec<(String, &dyn Fn(&T) -> f64)>,
) -> Vec<(String, String)> {
    let categories = T::categories(data);
    let clusters = T::clusters(data);

    let category_label = |x| {
        let label = T::category_label(x);
        cprop.legend_map.get(&label).cloned().unwrap_or(label)
    };

    let cluster_label = |c| {
        let cleaned = T::cluster_label(c).replace("/", "").replace("_", "");
        cprop.cluster_map.get(&cleaned).cloned().unwrap_or(cleaned)
    };

    let table_spec = (0..=value_fns.len()).map(|_| String::from("l")).join(" | ");
    let table_headers_inner = value_fns
        .iter()
        .map(|(header, _)| format!("\\textbf{{{header}}}"))
        .join(" & ");
    let table_headers = format!("\\textbf{{Method}} & {table_headers_inner} \\\\");

    let grids_relative = value_fns
        .iter()
        .map(|(name, f)| (name, T::build_grid(data, f, true)))
        .collect::<HashMap<_, _>>();
    let grids_absolute = value_fns
        .iter()
        .map(|(name, f)| (name, T::build_grid(data, f, false)))
        .collect::<HashMap<_, _>>();

    clusters.iter().map(|cluster| {
        let inner = categories
            .iter()
            .map(|category| {
                let cols = value_fns
                    .iter()
                    .map(|(name, _)| {
                        let idx = (cluster.clone(), category.clone());
                        let value = grids_absolute[name][&idx];
                        let value_relative = grids_relative[name][&idx] * 100.;
                        format!("{value} ({value_relative:.1}\\%)")
                    })
                    .join(" & ");

                format!("{category_label} & {} \\\\", cols, category_label=(category_label)(category))
            })
            .join("\n");

        let code = format!(
            r#"
            \begin{{tabular}}{{{table_spec}}}
                {table_headers}
                \hline
                {inner}
            \end{{tabular}}
        "#
        );

        ((cluster_label)(cluster), code)
    })
    .collect()
}
*/
