use std::path::PathBuf;

use anyhow::Context;
use regex::Regex;
use structopt::{clap::arg_enum, StructOpt};

mod gnuplot;
mod latex;
mod synth_data;
mod traits;

use synth_data::{
    IfKindSynth, PartSynth, QuartusSynthData, SeedSynth, VivadoIfKindSynth, VivadoSynthData, NextpnrIfKindSynth, NextpnrSynthData,
};

use crate::{
    latex::{CommonPlotProperties, SpecialProperties},
    synth_data::WordLengthSynth,
};

arg_enum! {
    #[derive(StructOpt)]
    enum ClusterKind {
        Seed,
        Part,
        WordLength,
        IfKind,
        VivadoIfKind,
        NextpnrIfKind
    }
}

#[derive(StructOpt, Debug)]
enum OutputKind {
    Gnuplot,
    Latex {
        #[structopt(short = "f")]
        out_figure_filename: PathBuf,
        #[structopt(short = "t")]
        out_table_filename: PathBuf,
        #[structopt(short = "p")]
        prop_file: PathBuf,
        #[structopt(short = "c")]
        common_prop_file: PathBuf,
    },
}

#[derive(StructOpt)]
struct Cmd {
    cluster_kind: ClusterKind,

    /// Input CSV file containing data to transform
    #[structopt()]
    csv: PathBuf,

    #[structopt(subcommand)]
    output_kind: Option<OutputKind>,
}

macro_rules! graph_kind {
    ($fn:ident, $synth_data:path, $wrapper_type:path, $wrapper_fn:path) => {
        fn $fn(reader: &mut csv::Reader<&[u8]>) -> anyhow::Result<Vec<$wrapper_type>> {
            let mut data = vec![];
            for result in reader.deserialize() {
                let record: $synth_data = result?;

                data.push($wrapper_fn(record))
            }
            Ok(data)
        }
    };
}

graph_kind!(part_clusters, QuartusSynthData, PartSynth, PartSynth::new);
graph_kind!(seed_clusters, QuartusSynthData, SeedSynth, SeedSynth::new);
graph_kind!(
    word_length_clusters,
    QuartusSynthData,
    WordLengthSynth,
    WordLengthSynth::new
);
graph_kind!(
    if_kind_clusters,
    QuartusSynthData,
    IfKindSynth,
    IfKindSynth::new
);
graph_kind!(
    vivado_if_kind_clusters,
    VivadoSynthData,
    VivadoIfKindSynth,
    VivadoIfKindSynth::new
);
graph_kind!(
    nextpnr_if_kind_clusters,
    NextpnrSynthData,
    NextpnrIfKindSynth,
    NextpnrIfKindSynth::new
);

fn main() -> anyhow::Result<()> {
    let cmd = Cmd::from_args();

    let file_content = std::fs::read_to_string(cmd.csv)?;

    // Trim away leading spaces in table
    let space_removal_regex = Regex::new(r" *, *")?;
    let stripped = space_removal_regex.replace_all(&file_content, ",");

    let mut reader = csv::ReaderBuilder::new()
        .delimiter(b',')
        .from_reader(stripped.as_bytes());

    macro_rules! do_plot {
        ($handler:path, $($field:ident),*) => {
            {
                let data = $handler(&mut reader)?;
                let mut processes = vec![];
                $(
                    let field = stringify!($field);
                    let filename = format!("{}.gnuplot", field);
                    std::fs::write(
                        &filename,
                        gnuplot::generate_gnuplot(&field, &data, |d| d.data.$field as f64, true)
                    )?;

                    println!("Updated {}", filename);

                    let child = std::process::Command::new("gnuplot")
                        .arg(filename)
                        .spawn()
                        .context("Failed to spawn gnuplot")?;

                    processes.push(child);
                )*

                for mut child in processes {
                    child.wait()?;
                }
            }
        }
    }

    match cmd.output_kind {
        Some(OutputKind::Gnuplot) | None => {
            match cmd.cluster_kind {
                ClusterKind::Seed => do_plot!(seed_clusters, alms),
                ClusterKind::Part => do_plot!(part_clusters, alms),
                ClusterKind::WordLength => do_plot!(word_length_clusters, alms),
                ClusterKind::IfKind => do_plot!(if_kind_clusters, alms, luts, luts_and_regs, regs),
                ClusterKind::VivadoIfKind => {
                    do_plot!(vivado_if_kind_clusters, slices, slice_luts, slice_registers)
                }
                ClusterKind::NextpnrIfKind => {
                    do_plot!(nextpnr_if_kind_clusters, slices)
                }
            };
        }
        Some(OutputKind::Latex {
            out_figure_filename,
            out_table_filename,
            prop_file,
            common_prop_file,
        }) => {
            macro_rules! load_props {
                ($filename:expr, $type:ident) => {{
                    let f = std::fs::read_to_string(&$filename)
                        .with_context(|| format!("Failed to read {:?}", $filename))?;

                    toml::from_str::<$type>(&f)
                        .with_context(|| format!("Failed to decoe {:?}", $filename))?
                }};
            }

            let cprop = load_props!(common_prop_file, CommonPlotProperties);
            let prop = load_props!(prop_file, SpecialProperties);

            macro_rules! do_latex {
                ($handler:path, $field:ident, $kind:ident) => {
                    {
                        let data = $handler(&mut reader)?;
                        std::fs::write(
                            &out_figure_filename,
                            latex::generate_tex_figure(
                                &prop.plot,
                                &cprop,
                                &data,
                                &|d| d.data.$field as f64
                            )
                        ).with_context(|| format!("failed to write to {out_figure_filename:?}"))?;

                        println!("Updated {}", out_figure_filename.to_string_lossy());

                        std::fs::write(
                            &out_table_filename,
                            latex::generate_tex_table(
                                &prop.table,
                                &cprop,
                                &data,
                                &|d| d.data.$field as f64
                            )
                        ).with_context(|| format!("failed to write to {out_table_filename:?}"))?;

                        println!("Updated {}", out_table_filename.to_string_lossy());
                    }
                }
            }

            match cmd.cluster_kind {
                ClusterKind::Seed => do_latex!(seed_clusters, alms, SeedSynth),
                ClusterKind::Part => do_latex!(part_clusters, alms, PartSynth),
                ClusterKind::WordLength => {
                    do_latex!(word_length_clusters, alms, WordLengthSynth)
                }
                ClusterKind::IfKind => {
                    do_latex!(if_kind_clusters, alms, IfKindSynth)
                }
                ClusterKind::VivadoIfKind => {
                    do_latex!(
                        vivado_if_kind_clusters,
                        slices,
                        VivadoIfKindSynth
                    )
                }
                ClusterKind::NextpnrIfKind=> {
                    do_latex!(
                        nextpnr_if_kind_clusters,
                        slices,
                        NextpnrIfKindSynth
                    )
                }
            }
        }
    }

    Ok(())
}
