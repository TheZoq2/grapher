use std::{collections::HashMap, fmt::Debug, hash::Hash};

use itertools::Itertools;

/*
|                              || category 1
|                              ** category 2
|             ||               ## category 3
|     **      ||
|     **      ||**        **
|   ||**      ||**##      **
|   ||**      ||**##      **##
|   ||**##    ||**##    ||**##
|   ||**##    ||**##    ||**##
+----------------------------------------------------
      c1        c2        c3        <-- Clusters
 */

pub trait Chart {
    type Cluster: Hash + Clone + Eq + Debug;
    type Category: Hash + Clone + Eq + Debug;

    /// Return the cluster where this value should be drawn
    fn cluster(&self) -> Self::Cluster;
    fn category(&self) -> Self::Category;

    /// Where should this datapoint be inserted
    fn location(&self) -> (Self::Cluster, Self::Category)
    where
        Self: Sized,
    {
        (self.cluster(), self.category())
    }

    fn cluster_label(raw_cluster: &Self::Cluster) -> String;
    fn category_label(raw_category: &Self::Category) -> String;

    // Methods with defaults

    fn sort_clusters(clusters: Vec<Self::Cluster>) -> Vec<Self::Cluster> {
        clusters
    }

    // Provided methods which are normally not meant to be overwritten

    fn gnuplot_data(
        values: &[Self],
        value_fn: impl Fn(&Self) -> f64,
        relative_values: bool,
    ) -> String
    where
        Self: Sized,
    {
        let grid = Self::build_grid(values, value_fn, relative_values);

        let category_max = Self::clusters(values)
            .iter()
            .cloned()
            .map(|cluster| {
                let max = Self::categories(values)
                    .iter()
                    .map(|category| {
                        *grid
                            .get(&(cluster.clone(), category.clone()))
                            .unwrap_or(&0.)
                    })
                    .fold(-f64::INFINITY, |a, b| a.max(b));

                (cluster, max)
            })
            .collect::<HashMap<_, _>>();

        let mut lines = vec![];
        for cluster in Self::clusters(values) {
            let mut datapoints = vec![];
            for category in Self::categories(values) {
                let value = grid.get(&(cluster.clone(), category)).unwrap_or(&0.);
                let value_scaled = value / category_max[&cluster];
                datapoints.push(format!("{}", value_scaled))
            }
            lines.push(format!(
                "{} {}",
                Self::cluster_label(&cluster),
                datapoints.join(" ")
            ))
        }

        lines.join("\n")
    }

    fn build_grid(
        data: &[Self],
        value_fn: impl Fn(&Self) -> f64,
        relative_values: bool,
    ) -> HashMap<(Self::Cluster, Self::Category), f64>
    where
        Self: Sized,
    {
        let mut raw_grid = HashMap::new();
        for value in data {
            if let Some(_) = raw_grid.insert(value.location(), (value_fn)(value)) {
                panic!("Found a multiple values for {:?}", value.location())
            }
        }

        if relative_values {
            let category_max = Self::clusters(data)
                .iter()
                .cloned()
                .map(|cluster| {
                    let max = Self::categories(data)
                        .iter()
                        .map(|category| {
                            *raw_grid
                                .get(&(cluster.clone(), category.clone()))
                                .unwrap_or(&0.)
                        })
                        .fold(-f64::INFINITY, |a, b| a.max(b));

                    (cluster, max)
                })
                .collect::<HashMap<_, _>>();

            let mut result = HashMap::new();
            for value in data {
                let raw_value = (value_fn)(value);
                let val = raw_value / category_max[&value.location().0];
                if let Some(_) = result.insert(value.location(), val) {
                    panic!("Found a multiple values for {:?}", value.location())
                }
            }
            result
        } else {
            raw_grid
        }
    }

    /// Returns the unique categories that exist for the dataset
    fn clusters(values: &[Self]) -> Vec<Self::Cluster>
    where
        Self: Sized,
    {
        Self::sort_clusters(values.iter().map(Chart::cluster).unique().collect())
    }

    /// Returns the unique categories that exist for the dataset
    fn categories(values: &[Self]) -> Vec<Self::Category>
    where
        Self: Sized,
    {
        values.iter().map(Chart::category).unique().collect()
    }
}
